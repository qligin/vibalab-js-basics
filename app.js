// Примеры переменных
var hello_string = "Hello";
var hundred = 100;
var not_false = true;
var cats = ["Boris", "Vasiliy", "Barsik"];
var dogs = new Array("Sharik", "Tolik", "Bucks");

console.log(typeof(cats));
console.log(typeof(dogs));

console.log('Арифметические операции');
var a = 10;
var b = 15;
a += b;
console.log(a);

a -= b;
console.log(a);

a /= b;
console.log(a);

a = 40
b *= a;
console.log(a);

a = 5;
b = 3;
a %= b;
console.log(a);
console.log('\n');


a = 3;
b = '3';
var c = 15;

console.log('Операторы сравнения');
console.log('\n');
console.log(a == b);
console.log(a === b);
console.log('\n');

console.log(a != b);
console.log(a !== b);
console.log('\n');

console.log(a > b);
console.log(a >= c);
console.log('\n');

console.log('Операции с массивами');
var arr1 = new Array("Hi.", "My", "name", "is", "Jimmy");
var arr2 = new Array("krab gennadiy", 23, "(\\/)_(-_-)_(\\/)");

delete arr1[3];
console.log(arr1);
var sum_array = arr1 + arr2;
console.log(sum_array);

